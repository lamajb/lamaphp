<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Interview;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();

        return view('interviews.index',compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $userId = Auth::id();
        $user = User::findOrFail($userId);
        if ($user->isAdmin()|| $user->isManager()){
            $candidates = Candidate::all();
            $users = User::all();
            $users = $users->sortBy(function($model)use ($user) {
                return $model->getKey() != $user->id;
            });
            return view('interviews.create', compact('candidates','users'));
        }
        abort(403,"Sorry you are not allowed to add interviews");  
    }

    
    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;   
        return view('interviews.index',compact('interviews'));
    }


    public function changeUser($cid, $uid = null){
        $candidates = Candidate::all();
        $users = User::all();
        $target = Candidate::findOrFail($cid);
        $target = $target->owner();
     
        return view('interviews.create', compact('candidates','users'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $candidate = new Interview();
        $can = $candidate->create($request->all());
        $can->save();
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
