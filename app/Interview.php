<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'shortsum', 'candidate','user',
    ];

    public function person(){
        return $this->belongsTo('App\Candidate','candidate');
    }
    public function interviewer(){
        return $this->belongsTo('App\User','user');
    }

}
