@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "shortsum">sum of the interviews</label>
            <input type = "text" class="form-control" name = "shortsum">
        </div>     
        <div class="form-group">
            <label for = "date">date</label>
            <input type = "date" class="form-control" name = "date">
        </div> 
        <div class="form-group">
            <label for="candidate">candidate</label>
            <br>
            <select name="candidate" id="candidate">
                @foreach($candidates as $candidate)
                    <option value="{{$candidate->id}}">{{$candidate->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="user">supervisor</label>
            <br>
            <select name="user" id="user">
                @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
        </div>


        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
