@extends('layouts.app')

@section('title', 'interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

@if (auth()->user()->checkedInToday())
<div><a href =  "{{url('/interviews/create')}}"> Add new one</a></div>
@endif

@if(count($interviews) >= 1 )

<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>interviewer</th><th>interviewee</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->shortsum}}</td>
            <td>{{$interview->date}}</td>
            <td>
                @if(isset($interview->interviewer))
                    {{$interview->interviewer->name}}
                @endif
            </td>
            <td>
                @if(isset($interview->person))
                    {{$interview->person->name}}
                @endif
            </td>              
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>                                  
        </tr>
    @endforeach
</table>
@else
    you have no interviews
@endif
@endsection

