<?php

use Illuminate\Database\Seeder;

class InterviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('interviews')->insert([
            [
                'shortsum' => 'the persson is smart and good',
                'date' => '2020-6-9',
            ],
            [
                'shortsum' => 'the person is nice',
                'date' => '2015-3-7',
            ],                 
        ]); 
    }
}
